# Preview all emails at http://localhost:3000/rails/mailers/auto_mailer
class AutoMailerPreview < ActionMailer::Preview
  def sample_mail_preview
    AutoMailer.sample_email(User.find(2))
  end
end
