class AddPictureToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :picture, :string
    add_column :blogs, :show_info, :boolean
  end
end
