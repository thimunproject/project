class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer :user_id
      t.string :title
      t.text :body
      t.string :recp

      t.timestamps null: false
    end
  end
end
