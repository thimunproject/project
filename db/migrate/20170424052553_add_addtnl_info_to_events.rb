class AddAddtnlInfoToEvents < ActiveRecord::Migration
  def change
    add_column :events, :addtnl_info, :text
  end
end
