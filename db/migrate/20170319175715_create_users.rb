class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :fname
      t.string :lname
      t.string :email
      t.string :password_digest
      t.string :school
      t.string :city
      t.string :counrty
      t.string :occupation
      t.boolean :debate_experience
      t.boolean :familiar

      t.timestamps null: false
    end
  end
end
