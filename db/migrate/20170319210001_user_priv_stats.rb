class UserPrivStats < ActiveRecord::Migration
  def change
    add_column :users, :priv_lvl, :integer
    add_column :users, :active, :boolean
    add_column :users, :rcv_emails, :boolean    
  end
end
