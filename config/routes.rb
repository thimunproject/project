Rails.application.routes.draw do
  resources :contacts
  resources :attendees
  resources :events do
    post :attend, on: :member
    post :leave, on: :member
  end
  resources :blogs
  resources :users do
    get :approve, on: :member
  end
  root to: 'visitors#index'
  #get "/pages/:page" => "pages#show"
  get '/welcome' => "pages#welcome"
  get '/community' => "pages#community"
    get '/videos' => "pages#videos"
  get '/contact' => "pages#contact"
    get '/fun' => "pages#fun"
  get '/general-education' => "pages#general-education"
    get '/troubleshooting' => "pages#troubleshooting"
  get '/Security' => "pages#Security"
  get '/debate-info' => "pages#debate-info"
  get '/Assembly' => "pages#Assembly"
  get '/court' => "pages#court"
  get '/special' => "pages#special"
  get '/jro-mun' => "pages#jro-mun"
  get '/advise' => "pages#advise"
    get '/internationalAffairs' => "pages#internationalAffairs"

 get '/community' => "pages#community"
 #get '/debate-info' => "pages#debate-info"

  get '/omunresources' => "pages#omunresources"
  get '/contact' => "pages#contact"
    get '/hela' => "pages#hela"
  get '/thimun_qatar' => "pages#thimun_qatar"
  
  get '/thimun-o-mun-rules-and-procedures' => "pages#thimun-o-mun-rules-and-procedures"
  
  # pages included in thimun o mun rules and procedures
  get '/speech-making-and-forms-address' => "pages#speech-making-and-forms-address"
  get '/opening-speeches-policy-statements-and-draft-resolutions' => "pages#opening-speeches-policy-statements-and-draft-resolutions"
  get '/commonly-used-terms' => "pages#commonly-used-terms"
  get '/debate-procedure' => "pages#debate-procedure"
  get '/specific-rules-and-procedures-online-debates' => "pages#specific-rules-and-procedures-online-debates"
  get '/draft-resolution' => "pages#draft-resolution"
  get '/words-and-phrases-drafting-resolutions' => "pages#words-and-phrases-drafting-resolutions"
  get '/stock-chairing-phrases' => "pages#stock-chairing-phrases"

  
  get '/writing-thimun-resolution' => "pages#writing-thimun-resolution"
  
  # pages included in writing thimun resolution
  get '/purpose-resolution' => "pages#purpose-resolution"
  get '/heading' => "pages#heading"
  get '/first-line' => "pages#first-line"
  get '/preambulatory-clauses' => "pages#preambulatory-clauses"
  get '/operative-clauses' => "pages#operative-clauses"
  get '/sample-resolution' => "pages#sample-resolution"
  

  
  get '/thimun-o-mun-delegate-guide' => "pages#thimun-o-mun-delegate-guide"
  
  # pages included in thimun o mun delegate guide 
    
  get '/thimun-o-mun-vision' => "pages#thimun-o-mun-vision"
  get '/introduction' => "pages#introduction"
  get '/technology' => "pages#technology"
  get '/debating-blackboard-collaborate' => "pages#debating-blackboard-collaborate"
  get '/debate-procedure-step-step' => "pages#debate-procedure-step-step"
  get '/rules-procedures-online-equivalents' => "pages#rules-procedures-online-equivalents"
  get '/debate-cycles-country-assignments' => "pages#debate-cycles-country-assignments"
  get '/moderating-chairing' => "pages#moderating-chairing"

  
  get '/power-search' => "pages#power-search" 
  get '/un-guide-model-united-nations' => "pages#un-guide-model-united-nations" 

  
  
  
  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'

  get '/signup' => 'users#new'
  post '/users' => 'users#create'
  
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
