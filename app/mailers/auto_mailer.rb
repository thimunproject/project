class AutoMailer < ApplicationMailer
    default from: "thimunmailingbot@gmail.com"
    
    def sample_email(user, new_user)
        @user = user
        @new_user = new_user
        mail(to: @user.email, subject: 'Thimun Website - New User Notification')
    end
    
    def contact_email(contact)
        @contact = contact
        if @contact.recp == "Saqib Mahmoud"
            mail(to: "ahmed.sameh.abdultawab@gmail.com", subject: 'Thimun Website - Contact Us Message')
        elsif @contact.recp == "Lisa Martin"
            mail(to: "ahmed.sameh.abdultawab@gmail.com", subject: 'Thimun Website - Contact Us Message')
        elsif @contact.recp == "Both"
            mail(to: "aabdulta@cmu.edu", subject: 'Thimun Website - Contact Us Message')
            mail(to: "ahmed.sameh.abdultawab@gmail.com", subject: 'Thimun Website - Contact Us Message')
        end
    end
end
