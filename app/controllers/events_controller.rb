class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :leave]
  before_action :authorize, only: [ :edit, :update, :create, :new, :destroy]

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
  end

  # GET /events/1
  # GET /events/1.json
  def show
    
    respond_to do |format|
        begin
          format.html
          format.xlsx {
          render xlsx: 'events', template: 'events/show', filename: "#{@event.name} Event Attendees List.xlsx"
          }
          
          rescue TypeError
            redirect_to customer_request_path, notice: 'Couldn\'t Generate Attendees List'
          end
      end
      
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.user_id = current_user.id
    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end
  

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def attend
  
    @attendee = Attendee.new do |c|
      c.user_id = current_user.id
      c.event_id = params[:id]
    end
    
    if @attendee.save
      flash[:alert]= 'You will be attending this event'
      redirect_to event_path
    else
      flash[:alert]= 'Error Attending Event'
      redirect_to event_path
    end
    
  end
  
  def leave
    
    @attendee = Attendee.find_by(user_id: current_user.id, event_id: @event.id)
    @attendee.destroy
    respond_to do |format|
      format.html { redirect_to event_path, notice: 'You won\'t be attending this event' }
      format.json { head :no_content }
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end
    
    def authorize
      if !current_user
         redirect_to :login , :alert => "You must be logged in to access this page"
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :user_id, :topic, :commitee, :addtnl_info, :rgstr_end, :start_time, :end_time)
    end
end
