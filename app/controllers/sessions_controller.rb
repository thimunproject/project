class SessionsController < ApplicationController
  skip_before_action :authenticate
  
  def new
  end
  
  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to :root #CHANGE ME LATER
    else
      flash.now[:alert] = 'Invalid email/password'
      redirect_to :root
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to '/users'
  end

end
