class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :approve]
  before_action :authorize, only: [:index, :edit, :update, :destroy, :approve]
  skip_before_action :authenticate, only: [:new, :create]


  # GET /users
  # GET /users.json
  def index
    if current_user.priv_lvl < 2
      redirect_to :root , :alert => "You can only edit your own profile"
    end
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    if current_user.priv_lvl < 2 && current_user != @user
      redirect_to :root , :alert => "You can only edit your own profile"
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.active = false
    @user.priv_lvl = 0
    respond_to do |format|
      if @user.save
        
        # Sends email to user when user is created.
        AutoMailer.sample_email(User.find(2), @user).deliver_now
        if current_user 
          format.html { redirect_to users_url, notice: 'User was successfully created.' }
          format.json { render :show, status: :created, location: @user }
        else
          format.html { redirect_to "/welcome" }
        end
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def approve
    @user.active = true
    @user.priv_lvl = 1
    respond_to do |format|
      if @user.save
        format.html { redirect_to users_url, notice: 'User was successfully Approved.' }
        format.json { head :no_content }
       else
        flash[:alert]= 'Error Approving'
        redirect_to users_path
      end
    end
    
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end
    
    def authorize
      if !current_user
         redirect_to :login , :alert => "You must be logged in to access this page"
      end
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:fname, :lname, :email, :password, :password_confirmation, :avatar, :school, :city, :counrty, :occupation, :debate_experience, :familiar, :priv_lvl, :active, :rcv_emails)
    end
end
