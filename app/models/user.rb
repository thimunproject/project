class User < ActiveRecord::Base
    mount_uploader :avatar, AvatarUploader
    has_secure_password
    
    
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
     
    validates :fname, :lname, presence: true
    validates :password, :password_confirmation, presence: true
    validates :email, presence: true, length: { minimum: 6, maximum: 255 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }
end
