class Blog < ActiveRecord::Base
        belongs_to :user, foreign_key: :user_id
        mount_uploader :picture, EvntpictureUploader
        
        validates :title, :body, presence: true
end
