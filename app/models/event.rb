class Event < ActiveRecord::Base
    belongs_to :user, foreign_key: :user_id
     
    validates :name, :topic, :start_time, :end_time, presence: true
end
