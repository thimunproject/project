json.extract! event, :id, :name, :user_id, :topic, :commitee, :rgstr_end, :start_time, :end_time, :created_at, :updated_at
json.url event_url(event, format: :json)