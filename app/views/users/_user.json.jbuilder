json.extract! user, :id, :fname, :lname, :email, :password_digest, :school, :city, :counrty, :occupation, :debate_experience, :familiar, :created_at, :updated_at
json.url user_url(user, format: :json)